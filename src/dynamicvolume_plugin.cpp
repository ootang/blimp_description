
#include "dynamicvolume_plugin.hpp"
// USER HEADERS
#include "ConnectGazeboToRosTopic.pb.h"


namespace gazebo{

  DynamicVolumePlugin::DynamicVolumePlugin()
      : ModelPlugin(),
        node_handle_(0),
        airdensity_(1.204),
        pubs_and_subs_created_(false){}

  DynamicVolumePlugin::~DynamicVolumePlugin(){}

  void DynamicVolumePlugin::Load(physics::ModelPtr _model,
                                    sdf::ElementPtr _sdf)
  {
    if (kPrintOnUpdates) {
      gzdbg << __FUNCTION__ << "() called." << std::endl;
    }

    gzdbg << "_model = " << _model->GetName() << std::endl;

    // Store the pointer to the model
    model_ = _model;
    world_ = model_->GetWorld();

    //==============================================//
    //========== READ IN PARAMS FROM SDF ===========//
    //==============================================//

    // Use the robot namespace to create the node handle.
    if (_sdf->HasElement("robotNamespace"))
      namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
    else
      gzerr << "[DynamicVolumePlugin] Please specify a robotNamespace.\n";

    // Create nodehandle
    node_handle_ = transport::NodePtr(new transport::Node());

    // Initialize with default namespace (typically /gazebo/default/)
    node_handle_->Init();

    // Get the link name
    std::string link_name;
    if (_sdf->HasElement("linkName"))
      link_name = _sdf->GetElement("linkName")->Get<std::string>();
    else
      gzerr << "[DynamicVolumePlugin] Please specify a linkName.\n";
    // Get the pointer to the link
    link_ = model_ ->GetLink(link_name);
    if (link_ == NULL){
      gzthrow("[DynamicVolumePlugin] Couldn't find link\""
              << link_name << "\".");
    }

    frame_id_ = link_name;

    // Retrieve the rest of the SDF parameters.
    getSdfParam<std::string>(_sdf, "dynamicvolumetopic", dynamic_volume_topic_, kDefaultDynamicVolumePubTopic);
    getSdfParam<double>(_sdf, "referenceAltitude", ref_alt_, kDefaultRefAlt);
    getSdfParam<double>(_sdf, "pressureVariance", pressure_var_, kDefaultPressureVar);
    CHECK(pressure_var_ >= 0.0);

    // Initialize the normal distribution for pressure.
    double mean = 0.0;
    pressure_n_[0] = NormalDistribution(mean, sqrt(pressure_var_));

    // Listen to the update event. This event is broadcast every
    // simulation iteration.
    this->updateConnection_ = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&DynamicVolumePlugin::OnUpdate, this, _1));

    //==============================================//
    //=== POPULATE STATIC PARTS OF PRESSURE MSG ====//
    //==============================================//
    dynamic_volume_message_.mutable_header()->set_frame_id(frame_id_);
    dynamic_volume_message_.set_variance(pressure_var_);
  }

  void DynamicVolumePlugin::OnUpdate(const common::UpdateInfo& _info) {
    if (kPrintOnUpdates) {
      gzdbg << __FUNCTION__ << "() called." << std::endl;
    }

    if (!pubs_and_subs_created_) {
      CreatePubsAndSubs();
      pubs_and_subs_created_ = true;
    }

    common::Time current_time = world_->SimTime();

    // Get the current geometric height.
    double height_geometric_m = ref_alt_ + model_->WorldPose().Pos().Z();

    // Compute the geopotential height.
    double height_geopotential_m = kEarthRadiusMeters * height_geometric_m /
        (kEarthRadiusMeters + height_geometric_m);

    // Compute the temperature at the current altitude.
    double temperature_at_altitude_kelvin =
        kSeaLevelTempKelvin - kTempLapseKelvinPerMeter * height_geopotential_m;

    // Compute the current air pressure.
    double pressure_at_altitude_pascal =
        kPressureOneAtmospherePascals * exp(kAirConstantDimensionless *
            log(kSeaLevelTempKelvin / temperature_at_altitude_kelvin));

    // Add noise to pressure measurement.
    if(pressure_var_ > 0.0) {
      pressure_at_altitude_pascal += pressure_n_[0](random_generator_);
    }

    // Compute the volume of the helium at current pressure and temperature by V = KT/P where K = nR
    double volume_at_altitude_meter3 = kHeliumKmolKr * temperature_at_altitude_kelvin / pressure_at_altitude_pascal;

    // Fill the pressure message.
    dynamic_volume_message_.mutable_header()->mutable_stamp()->set_sec(
        current_time.sec);
    dynamic_volume_message_.mutable_header()->mutable_stamp()->set_nsec(
        current_time.nsec);
    dynamic_volume_message_.set_fluid_pressure(volume_at_altitude_meter3);

    // Publish the pressure message.
    dynamic_volume_pub_->Publish(dynamic_volume_message_);

    UpdateForcesAndMoments(volume_at_altitude_meter3, link_);
  }

  void DynamicVolumePlugin::UpdateForcesAndMoments(double volume, physics::LinkPtr link){

    // caculate buoyancy
    const ignition::math::Vector3d buoyancy =
    ignition::math::Vector3d (0, 0, airdensity_ * volume * kGravityMagnitude);

    ignition::math::Pose3d linkFrame = link->WorldPose();

    // rotate buoyancy into the link frame before applying the force.
    ignition::math::Vector3d buoyancyLinkFrame =
        linkFrame.Rot().Inverse().RotateVector(buoyancy);

    link_->AddRelativeForce(buoyancyLinkFrame);

  }

  void DynamicVolumePlugin::CreatePubsAndSubs(){
    // ============================================ //
    // ========= FLUID PRESSURE MSG SETUP ========= //
    // ============================================ //
    dynamic_volume_pub_ = node_handle_->Advertise<gz_sensor_msgs::FluidPressure>(
        "~/" + namespace_ + "/" + dynamic_volume_topic_, 1);

    gazebo::transport::PublisherPtr connect_gazebo_to_ros_topic_pub =
        node_handle_->Advertise<gz_std_msgs::ConnectGazeboToRosTopic>(
            "~/" + kConnectGazeboToRosSubtopic, 1);

    dynamic_volume_pub_ = node_handle_->Advertise<gz_sensor_msgs::FluidPressure>(
        "~/" + namespace_ + "/" + dynamic_volume_topic_, 1);

    gz_std_msgs::ConnectGazeboToRosTopic connect_gazebo_to_ros_topic_msg;
    connect_gazebo_to_ros_topic_msg.set_gazebo_topic("~/" + namespace_ + "/" +
                                                     dynamic_volume_topic_);
    connect_gazebo_to_ros_topic_msg.set_ros_topic(namespace_ + "/" +
                                                  dynamic_volume_topic_);
    connect_gazebo_to_ros_topic_msg.set_msgtype(
        gz_std_msgs::ConnectGazeboToRosTopic::FLUID_PRESSURE);
    connect_gazebo_to_ros_topic_pub->Publish(connect_gazebo_to_ros_topic_msg,
                                             true);
  }

  GZ_REGISTER_MODEL_PLUGIN(DynamicVolumePlugin);
}
